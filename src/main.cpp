#include <iostream>
#include "BinaryNode.h"
using namespace std;


int main(int argc, char** argv) {
	BinaryTree *bt = new BinaryTree();
	
	bt->Add(7);
	bt->Add(27);
	bt->Add(17);
	bt->Add(5);
	bt->Add(2);
	bt->Add(4);
	
	bt->PrintPreOrder();
	bt->PrintInOrder();
	bt->PrintPostOrder();
	
	return 0;
}
