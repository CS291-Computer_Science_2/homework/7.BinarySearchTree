#ifndef BINARYNODE_H
#define BINARYNODE_H
#include <iostream>
#include "stdlib.h"
using namespace std;

class BinaryTree {
	public:
		BinaryTree() : root(nullptr){
			root=NULL;
		}
		void Add(int val);
		void PrintInOrder();
		void PrintPreOrder();
		void PrintPostOrder();

	private:
		struct BinaryNode {
			int value;
			BinaryNode *LeftChild;
			BinaryNode *RightChild;
		};
		
		BinaryNode *root; //Tracts top of element
		void Insert(BinaryNode *&node, int val);
		void PreOrder(BinaryNode *node);
		void InOrder(BinaryNode *node);
		void PostOrder(BinaryNode *node);
};

#endif
