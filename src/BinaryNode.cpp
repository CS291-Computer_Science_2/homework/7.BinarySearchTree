#include <iostream>
#include "BinaryNode.h"
using namespace std;

void BinaryTree::Add(int val) {
	Insert(root,val);
}

void BinaryTree::Insert(BinaryNode *&node, int val) {
	/*CREATE NEW TREE AND INITIALIZE MEMBERS, including pointers*/
	if(node==nullptr) { //true on first run, not created yet
		BinaryNode *newptr = new BinaryNode; //instantiate a new struct
		newptr->value= val; //add value to struct
		newptr->LeftChild= nullptr;
		newptr->RightChild= nullptr;
		printf("\n  Node created with value: %d.\n",val);
		node= newptr; //points to new object
		return;
	}

	/*CHECK FOR DUPLICATES*/
	if (val==node->value) {
		cout<<"Value already in tree\n";
		return;
	}

	/*SMALLER values pushed to the left of top*/
	if(val< (node->value))
		Insert(node->LeftChild, val);


	/*LARGER values pushed to the right of top*/
	else
		Insert (node->RightChild, val);


}

void BinaryTree::PrintPreOrder() {
	printf("\n Pre Order printing: ");
	PreOrder(root);
}
/*(VLR)Value->Left->Right*/
void BinaryTree::PreOrder(BinaryNode *node) {
	if(node==nullptr) return;

	//Print Value
	cout<<" "<<node->value;

	//visit the left child
	PreOrder(node->LeftChild);

	//visit the right child
	PreOrder(node->RightChild);
}

void BinaryTree::PrintInOrder() {
	printf("\n In Order printing: ");
	InOrder(root);
}

/*(LVR) Left->Value->Right*/
void BinaryTree::InOrder(BinaryNode *node) {
	if(node==nullptr) return;

	//visit the left child
	InOrder(node->LeftChild);

	//Print Value
	cout<<" "<<node->value;

	//visit the right child
	InOrder(node->RightChild);

}

void BinaryTree::PrintPostOrder() {
	printf("\n Post Order printing: ");
	PostOrder(root);
}

/*(LRV) Left->Right->Value*/
void BinaryTree::PostOrder(BinaryNode *node) {
	if(node==nullptr) return;

	//visit the left child
	PostOrder(node->LeftChild);

	//visit the right child
	PostOrder(node->RightChild);

	//Print Value
	cout<<" "<<node->value;
}
